import tempfile


class Point(object):
    def __init__(self, x=0, y=0,):
        self.x = x
        self.y = y

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Point(x, y)

    def show(self):
        print(f"Coordinates: X={self.x}, Y={self.y}")


def save_to_file(obj, file_path):
    import json
    with open(file_path, "w") as f:
        json.dump(obj.__dict__, f)


def load_from_file(file_path):
    import json
    with open(file_path, "r") as f:
        return Point(**json.load(f))


p1 = Point(1, 3)
tmp_file = tempfile.NamedTemporaryFile()
save_to_file(p1, tmp_file.name)
p1 = Point(2, -2)
p2 = load_from_file(tmp_file.name)
p3 = p1 + p2

print("p1:")
p1.show()
print("p2:")
p2.show()
print("p3 = p1 + p2:")
p3.show()
