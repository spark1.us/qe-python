import tempfile
from main import (Point, save_to_file, load_from_file)


def test_serialization():
    tmp_file = tempfile.NamedTemporaryFile()
    p1 = Point(7, 8)
    save_to_file(p1, tmp_file.name)
    p2 = load_from_file(tmp_file.name)
    assert p1.x == p2.x and p1.y == p2.y


def test_serialization_security():
    import os

    tmp_file = tempfile.NamedTemporaryFile()
    malicious_file = "/tmp/vulnerable.txt"

    class Malicious(object):
        def __init__(self, json_data=None):
            pass

        def __reduce__(self):
            # malicious_cmd = "rmr -rf /"
            malicious_cmd = f"echo \"Arbitrary command execution from Python object deserialization\" > {malicious_file}"
            return os.system, (malicious_cmd,)

    if os.path.exists(malicious_file):
        os.remove(malicious_file)

    malicious_obj = Malicious()
    save_to_file(malicious_obj, tmp_file.name)
    p2 = load_from_file(tmp_file.name)
    assert os.path.exists(malicious_file) == False


def test_sum():
    p1 = Point(1, 1)
    p2 = Point(2, 2)
    p3 = p1+p2
    assert p3.x == 3 and p3.y == 3

